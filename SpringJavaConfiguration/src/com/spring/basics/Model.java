package com.spring.basics;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Model {

	@Value(value = "ThinkPad")
	private String name;
	
	@Value(value = "Black")
	private String color;
	
	@Value(value = "500GB HDD,8Gb RAM,i7 processor")
	private String specifications;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getSpecifications() {
		return specifications;
	}
	public void setSpecifications(String specifications) {
		this.specifications = specifications;
	}
	
	@Override
	public String toString() {
		return "Model [name=" + name + ", color=" + color + ", specifications=" + specifications + "]";
	}
	
	
}
