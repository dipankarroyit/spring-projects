package com.spring.basics;

public class BookTicket {
	
	private String ticketNumber;
	private String from;
	private String to;
	
	public BookTicket() {
		System.out.println(this.getClass().getSimpleName() + " object created");
	}
	
	public void booking() {
		System.out.println("Ticket Number: "+ this.ticketNumber +" from: "+ this.from +" to: "+ this.to +" has booked successfully!");
	}

	public BookTicket(String ticketNumber, String from, String to) {
		super();
		this.ticketNumber = ticketNumber;
		this.from = from;
		this.to = to;
	}

}
