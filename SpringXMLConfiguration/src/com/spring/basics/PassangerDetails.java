package com.spring.basics;

public class PassangerDetails {
	
	private String name;
	private String age;
	
	public PassangerDetails(String name, String age) {
		super();
		this.name = name;
		this.age = age;
	}

	@Override
	public String toString() {
		return "PassangerDetails [name=" + name + ", age=" + age + "]";
	}
		
}
