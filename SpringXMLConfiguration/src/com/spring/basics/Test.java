package com.spring.basics;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {

	public static void main(String[] args) {
		
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spring-web.xml");
		BookTicket bean = context.getBean(BookTicket.class);
	    //PassangerDetails bean1 = context.getBean(PassangerDetails.class);
		System.out.println(bean);
		//System.out.println(bean1);
		//bean.booking()

	}

}
