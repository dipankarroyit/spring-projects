package com.spring.basics;

 import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {

	public static void main(String[] args) {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spring-web.xml");
		BookTicket bean = context.getBean(BookTicket.class);
		bean.booking();
	}

}
