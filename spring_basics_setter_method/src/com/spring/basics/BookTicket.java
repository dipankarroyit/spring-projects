package com.spring.basics;

public class BookTicket {
	
	private String ticketNumber;
	private String from;
	private String to;
	
	public BookTicket() {
		System.out.println(this.getClass().getSimpleName() + " object created");
	}
	
	public void booking() {
		System.out.println("Ticket Number: "+ this.ticketNumber +" from: "+ this.from +" to: "+ this.to +" has booked successfully!");
	}

	public String getTicketNumber() {
		return ticketNumber;
	}

	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}
	
	
}
