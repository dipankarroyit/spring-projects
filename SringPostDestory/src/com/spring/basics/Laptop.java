package com.spring.basics;

import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Laptop {

	@Value(value = "Lenovo")
	private String name;

	@Value(value = "100000.500")
	private Double price;
	
	@Value(value = "Windows 10")
	private String osType;
	
	@Autowired
	private Model model;
	
	@PostConstruct
	public void postConstructDemo() {
		System.out.println("postConstructDemo mehtod is executing!!");
	}

	
	public Laptop() {
			System.out.println(this.getClass().getSimpleName() +" object is created");
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getOsType() {
		return osType;
	}

	public void setOsType(String osType) {
		this.osType = osType;
	}

	public Model getModel() {
		return model;
	}

	public void setModel(Model model) {
		this.model = model;
	}

	@Override
	public String toString() {
		return "Laptop [name=" + name + ", price=" + price + ", osType=" + osType + ", model=" + model + "]";
	}


}
